var ajaxreq = new AjaxRequest ();
AjaxRequest.prototype.send = function(type, url, handler, postDataType, postData) {
    
    if (this.request != null) {
        //kill the previous request
        this.request.abort();

        //tack on a dummy parameter to override browser caching
        url += "?dummy=" + new Date().getTime();

        try {
            this.request.onreadystatechange = handler;
            /*the custom handler fct will get called to manage
            the server response to the request*/
            this.request.open (type, url, true);
            //always asynchronous - true


                    //type argument of send() = GET or POST
                if (type.toLowerCase() == "get") {
                    //send a GET request, no data involved - null
                    this.request.send(null);
                     }
                else {
                    //send a POST request, the last argument is data
                    this.request.send(postData);
                    }
            }

         catch(e) {
                alert("Ajax error communicating with the server.\n" + "Details: " + e);
                }
         }
    }